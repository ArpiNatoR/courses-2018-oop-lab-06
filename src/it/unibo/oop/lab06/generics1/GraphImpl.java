package it.unibo.oop.lab06.generics1;

import static org.junit.Assert.fail;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

public class GraphImpl<N> implements Graph<N>{
	Map<N, Set<N>> graph = new HashMap<>();

	@Override
	public void addNode(N node) {
		if(!(graph.containsKey(node))){
			Set<N> nodeAdj = new TreeSet<>();
			graph.put(node, nodeAdj);
		}
		
	}

	@Override
	public void addEdge(N source, N target) {
		if(graph.containsKey(source)) {
			if(graph.containsKey(target)) {
				graph.get(source).add(target); //in una riga trova il set dei valori adiacenti e aggiunge target
				
			}else {
				fail("No target node found");
			}
			
		}else {
			fail("No source node found");
		}
	}

	@Override
	public Set<N> nodeSet() {
		return graph.keySet();
	}

	@Override
	public Set<N> linkedNodes(N node) {
		if(!(graph.containsKey(node))) {
			return null;
		}
		return graph.get(node); //ritorna la lista di adiacenza di quel nodo
	}

	@Override
	public List<N> getPath(N source, N target) {
		// TODO Auto-generated method stub
		return null;
	}

}
