package it.unibo.oop.lab.exception2;

import static org.junit.Assert.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.Test;

/**
 * JUnit test to test
 * {@link StrictBankAccount}.
 * 
 */
public class TestStrictBankAccount {

	static final int INITIAL_AMOUNT=10000;
	static final int MAX_ATM_TRANS=10;
	
    /**
     * Used to test Exceptions on {@link StrictBankAccount}.
     */
    @Test
    public void testBankOperations() {
        /*
         * 1) Creare due StrictBankAccountImpl assegnati a due AccountHolder a
         * scelta, con ammontare iniziale pari a 10000 e nMaxATMTransactions=10
         * 
         * 2) Effetture un numero di operazioni a piacere per verificare che le
         * eccezioni create vengano lanciate soltanto quando opportuno, cioè in
         * presenza di un id utente errato, oppure al superamento del numero di
         * operazioni ATM gratuite.
         */
    	
    	StrictBankAccount a1 = new StrictBankAccount(1, INITIAL_AMOUNT, MAX_ATM_TRANS);
    	StrictBankAccount a2 = new StrictBankAccount(2, INITIAL_AMOUNT, MAX_ATM_TRANS);
    	AccountHolder acc1 = new AccountHolder("Paola", "Avenali", 1);
    	AccountHolder acc2 = new AccountHolder("Nonno", "Nanni", 2);
    	
    	try 
    	{
    	a1.deposit(acc2.getUserID(), 200);//false
    	a2.deposit(acc2.getUserID(), 200);//true
    	a1.depositFromATM(acc1.getUserID(), 1000);//true
    	a1.depositFromATM(acc1.getUserID(), 10000);//true
    	a1.withdrawFromATM(acc1.getUserID(), 99999999);//false
    	}catch(Exception e) {
    		System.out.println(e.getMessage());
    	}
    	
    }
}
